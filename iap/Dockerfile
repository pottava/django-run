FROM python:3.10-slim

ENV APP_HOME /app
WORKDIR $APP_HOME

# Removes output stream buffering, allowing for more efficient logging
ENV PYTHONUNBUFFERED 1

# Install dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy local code to the container image.
COPY . .

RUN groupadd -r django
RUN useradd -r -g django -d ${APP_HOME} -s /sbin/nologin django
RUN chown -R django:django ${APP_HOME}
USER django

# Run the web service on container startup. Here we use the gunicorn
# webserver, with one worker process and 8 threads.
CMD gunicorn --bind 0.0.0.0:8080 --workers 1 --threads 8 --timeout 0 sample.wsgi:application

# IAP による Django アプリケーションの保護

## ローカル起動

```sh
python manage.py runserver
```

ブラウザで http://127.0.0.1:8000/ を起動します。

## クラウド環境

利用する API の有効化

```sh
gcloud services enable compute.googleapis.com run.googleapis.com \
    cloudbuild.googleapis.com storage.googleapis.com artifactregistry.googleapis.com \
    certificatemanager.googleapis.com cloudresourcemanager.googleapis.com \
    iap.googleapis.com
```

### Cloud Run のデプロイ

Cloud Run サービスをデプロイし

```sh
gcloud run deploy django --source . --platform "managed" --region "asia-northeast1" \
    --ingress "internal-and-cloud-load-balancing" --cpu 1.0 --memory 256Mi
```

IAP からの接続のみを許可します。

```sh
project_num="$( gcloud projects describe "$( gcloud config get-value project )" \
    --format='value(projectNumber)' )"
gcloud beta services identity create --service "iap.googleapis.com"
gcloud run services add-iam-policy-binding django --region "asia-northeast1" \
    --member "serviceAccount:service-${project_num}@gcp-sa-iap.iam.gserviceaccount.com" \
    --role "roles/run.invoker"
```

### [グローバル外部 HTTP(S) ロードバランサ](https://cloud.google.com/load-balancing/docs/https?hl=ja) の作成

[サーバーレス NEG](https://cloud.google.com/load-balancing/docs/negs/serverless-neg-concepts?hl=ja) & バックエンドの設定

```sh
gcloud compute network-endpoint-groups create django-asia-northeast1 \
    --network-endpoint-type "serverless" \
    --cloud-run-service "django" --region "asia-northeast1"
gcloud compute backend-services create django-backend \
    --load-balancing-scheme "EXTERNAL_MANAGED" \
    --global
gcloud compute backend-services add-backend django-backend \
    --network-endpoint-group "django-asia-northeast1" \
    --network-endpoint-group-region "asia-northeast1" \
    --global
```

[Google マネージド証明書](https://cloud.google.com/certificate-manager/docs/deploy-google-managed-dns-auth) の発行

```sh
gcloud certificate-manager dns-authorizations create django-cert-auth \
    --domain "${DOMAIN_NAME}"
gcloud certificate-manager dns-authorizations create django-www-cert-auth \
    --domain "www.${DOMAIN_NAME}"
gcloud certificate-manager dns-authorizations describe django-cert-auth
gcloud certificate-manager dns-authorizations describe django-www-cert-auth
gcloud certificate-manager certificates create django-certs \
    --dns-authorizations "django-cert-auth,django-www-cert-auth" \
    --domains "${DOMAIN_NAME},www.${DOMAIN_NAME}"
watch -n 5 gcloud certificate-manager certificates describe "django-certs"
```

ターゲット プロキシの設定

```sh
gcloud certificate-manager maps create certificate-map
gcloud certificate-manager maps entries create certificate-map-entry \
    --map "certificate-map" --certificates "django-certs" \
    --hostname "www.${DOMAIN_NAME}"
gcloud compute url-maps create django-urlmap \
    --global --default-service "django-backend"
gcloud compute target-https-proxies create django-target \
    --global --url-map "django-urlmap" \
    --certificate-map "certificate-map"
```

ロードバランサの設定

```sh
gcloud compute addresses create django-ip \
    --global --network-tier "PREMIUM"
gcloud compute forwarding-rules create "django-forwarding-rules" \
    --load-balancing-scheme "EXTERNAL_MANAGED" \
    --network-tier "PREMIUM" --address "django-ip" \
    --target-https-proxy "django-target" --ports "443" \
    --global
```

## IAP の設定

OAuth についての同意を設定

```sh
gcloud iap oauth-brands create --application_title "django" \
    --support_email "${EMAIL_ADDRESS}"
gcloud iap oauth-clients create \
    "$( gcloud iap oauth-brands list --format 'json' | jq -r '.[0].name' )" \
    --display_name "django-client"
gcloud compute backend-services update django-backend \
    --iap "enabled,oauth2-client-id=${C_ID},oauth2-client-secret=${C_SECRET}" \
    --global
```

組織 "内" のメンバーからのアクセスを許可する場合は「ユーザーの種類」が「内部」であることを確認  
https://console.cloud.google.com/apis/credentials/consent

組織 "外" のメンバーからのアクセスを許可する場合は「ユーザーの種類」を「外部」に更新  
https://console.cloud.google.com/apis/credentials/consent

組織 "外" のメンバーからのアクセスを許可したい場合で、  
かつアプリケーションがテスト状態の場合は「テストユーザー」にユーザーを追加。

その上で

特定の誰かからのアクセスを許可したい場合は以下を実行

```sh
gcloud iap web add-iam-policy-binding \
    --role "roles/iap.httpsResourceAccessor" \
    --member "user:${EMAIL_ADDRESS}"
```

特定の Google Group からのアクセスを許可したい場合は以下を実行

```sh
gcloud iap web add-iam-policy-binding \
    --role "roles/iap.httpsResourceAccessor" \
    --member "group:${EMAIL_ADDRESS}"
```

認証されたすべてのユーザーからのアクセスを許可する場合は以下を実行

```sh
gcloud iap web add-iam-policy-binding \
    --role='roles/iap.httpsResourceAccessor' \
    --member='allAuthenticatedUsers'
```

### Azure AD での SSO をする場合の注意点

基本的な設定は [こちら](https://cloud.google.com/architecture/identity/federating-gcp-with-azure-ad-configuring-provisioning-and-single-sign-on?hl=ja) で実施済みだとして

- 連携されたユーザーも個別に `iap web add-iam-policy-binding` が必要。Google Group での運用がよさそう
- Cloud Run サービスごとに許可グループが分けられないため、必要に応じてプロジェクト単位で分離する必要がある

## リソースの削除

ロードバランサ

```sh
gcloud compute forwarding-rules delete "django-forwarding-rules" --global --quiet
gcloud compute addresses delete django-ip --global --quiet
gcloud compute target-https-proxies delete django-target --global --quiet
gcloud compute url-maps delete django-urlmap --global --quiet
gcloud compute backend-services delete django-backend --global --quiet
```

NEG

```sh
gcloud compute network-endpoint-groups delete django-asia-northeast1 --region "asia-northeast1" --quiet
```

証明書

```sh
gcloud certificate-manager maps entries delete certificate-map-entry \
    --map "certificate-map" --quiet
gcloud certificate-manager maps delete certificate-map --quiet
gcloud certificate-manager certificates delete django-certs --quiet
```

OAuth クライアント

```sh
gcloud iap oauth-clients delete "$( gcloud iap oauth-clients list \
    "$( gcloud iap oauth-brands list --format 'json' | jq -r '.[0].name' )" \
    --format 'json' | jq -r '.[] | select(.displayName == "django-client") | .name' )" \
    --quiet
```

import os
from django.http import HttpRequest, HttpResponse
from google.auth.transport import requests
from google.oauth2 import id_token


def validate_iap_jwt(iap_jwt, expected_audience):
    """Validate an IAP JWT.

    Args:
      iap_jwt: The contents of the X-Goog-IAP-JWT-Assertion header.
      expected_audience: The Signed Header JWT audience. See
          https://cloud.google.com/iap/docs/signed-headers-howto
          for details on how to get this value.
    Returns:
      (user_id, user_email, error_str).
    """
    try:
        decoded_jwt = id_token.verify_token(
            iap_jwt, requests.Request(), audience=expected_audience,
            certs_url='https://www.gstatic.com/iap/verify/public_key')
        return (decoded_jwt['sub'], decoded_jwt['email'], '')
    except Exception as e:
        return (None, None, '**ERROR: JWT validation error {}**'.format(e))


def get_project_id():
    project_id = os.getenv("GCP_PROJECT")

    if not project_id:  # > python37
        import urllib.request

        url = "http://metadata.google.internal/computeMetadata/v1/project/project-id"
        req = urllib.request.Request(url)
        req.add_header("Metadata-Flavor", "Google")
        project_id = urllib.request.urlopen(req).read().decode()

    if not project_id:
        raise ValueError("Could not get a value for PROJECT_ID")
    return project_id


def index(request: HttpRequest) -> HttpResponse:
    # jwt = request.META['x-goog-iap-jwt-assertion']
    # if jwt is None:
    #     return HttpResponse("Hello, world!")

    # expected_audience = '/projects/{}/global/backendServices/{}'.format(
    #     get_project_id(), "django-backend")
    # user_id, user_email, err = validate_iap_jwt(jwt, expected_audience)
    # if err:
    #     return HttpResponse('Error: {}'.format(err))
    # else:
    #     return HttpResponse('Hi, {} ({}).'.format(user_id, user_email))

    # Cloud Run does not contain the JWT header.
    if 'HTTP_X_GOOG_AUTHENTICATED_USER_EMAIL' in request.META:
        email = request.META['HTTP_X_GOOG_AUTHENTICATED_USER_EMAIL']
        return HttpResponse('Hi, {}.'.format(email.replace('accounts.google.com:', '')))
    return HttpResponse("Hello, world!")

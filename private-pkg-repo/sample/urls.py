import os
from django.urls import path
from django.http import HttpResponse
from . import views


def version(request):
    revision = os.environ.get("K_REVISION", "local")
    return HttpResponse(f"{revision}", content_type="text/plain")


urlpatterns = [
    path("healthz", version),
    path("", views.index, name="index"),
]

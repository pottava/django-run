# Artifact Registry によるプライベート パッケージ リポジトリの利用

## keyring による認証設定

Artifact Registry へのアクセス時に[適切な認証がされるよう設定](https://cloud.google.com/artifact-registry/docs/python/authentication?hl=ja)します。

```sh
pip install keyring
pip install keyrings.google-artifactregistry-auth
keyring --list-backends
gcloud auth login
gcloud artifacts print-settings python --repository demo --location asia-northeast1
cat << EOF >  $HOME/.pypirc
[distutils]
index-servers =
    demo

[demo]
repository: https://asia-northeast1-python.pkg.dev/$( gcloud config get-value project )/demo/
EOF
cat << EOF > pip.conf
[global]
index-url = https://asia-northeast1-python.pkg.dev/$( gcloud config get-value project )/demo/simple/
EOF
```

## パッケージのアップロード

Python リポジトリを作成したら

```sh
gcloud artifacts repositories create demo \
    --repository-format python --location asia-northeast1
```

ローカルで管理するパッケージをまとめ

```sh
mkdir dist
pip download --destination-directory dist -r requirements.txt
```

[アップロード](https://cloud.google.com/artifact-registry/docs/python/store-python?hl=ja)します。

```sh
pip install twine
twine upload --repository-url \
    "https://asia-northeast1-python.pkg.dev/$( gcloud config get-value project )/demo/" \
    dist/* --skip-existing
```

## ローカル起動

```sh
python manage.py runserver
```

ブラウザで http://127.0.0.1:8000/ を起動します。

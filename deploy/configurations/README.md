マイクロサービスの差分管理例
---

## Base イメージ

```sh
docker build -t sample:base-v0.1 -f src/Dockerfile .
```

## Web TOP のビルド

```sh
docker build -t sample:top-v0.1 -f deploy/configurations/web-top/Dockerfile .
```

import os
from django.contrib import admin
from django.urls import include, path
from django.http import HttpResponse


def version(request):
    version = os.environ.get("APP_VERSION", "unknown")
    # for Cloud Run
    # revision = os.environ.get("K_REVISION", "local")
    # return HttpResponse(f"{version}-{revision}", content_type="text/plain")
    return HttpResponse(f"{version}", content_type="text/plain")


urlpatterns = [
    path("health", version),
    path("", include("gpus.urls")),
]

# Django Run

## 開発環境

1. セットアップ

    ```sh
    cd src
    python -m venv .venv
    source .venv/bin/activate
    pip install --upgrade pip
    pip install -r requirements.txt
    ```

1. 環境変数ファイルの作成

    ```sh
    echo SECRET_KEY=$( cat /dev/urandom | LC_ALL=C tr -dc '[:alpha:]' \
        | fold -w 50 | head -n1 ) > .env
    ```

1. DB マイグレーション

    ```sh
    LOCAL_DEV=1 python manage.py migrate
    ```

1. Celery の起動

    ```sh
    celery -A mysite worker -l info
    ```

1. Django Web サーバーの起動

    ```sh
    python manage.py runserver
    ```

1.  単体テストの実行

    ```sh
    flake8 --ignore E501 --exclude .venv .
    LOCAL_DEV=1 python manage.py test
    ```

## ローカル k8s 環境

1.  ローカル k8s クラスタの起動

    Minikube の場合

    ```sh
    minikube start --kubernetes-version=v1.24.3
    ```

    k3d の場合

    ```sh
    k3d cluster create cluster --servers 1 --image rancher/k3s:v1.20.7-k3s11
    ```

1.  Cloud Code から `Run on Kubernetes` を実施
    初期起動には 5 分程度かかりますが、ログに
    `[main]Starting development server at http://0.0.0.0:8080/` と表示されたら

    ```sh
    open http://localhost:8080/celery
    ```

1.  DB マイグレーション

    ```sh
    kubectl exec -it "$( kubectl get pods -l app=web \
        --field-selector status.phase=Running -o name )" -- bash
    root@web-app-xxx:/app# LOCAL_DEV=1 python manage.py migrate
    ```

1.  PostgreSQL

    localhost:5432 でも接続できますが、k8s 内での接続確認をするならこちら

    ```sh
    kubectl run -it postgres-cli --rm --image bitnami/postgresql --restart Never -- bash
    postgres@postgres-cli:/$ psql -h postgresql -U postgres -d demo
    demo=# \dt
    demo=# select * from django_migrations;
    ```

1.  Redis

    こちらも localhost:6379 で接続できますが

    ```sh
    kubectl run -it redis-cli --rm --image redis --restart Never -- bash
    root@redis-cli:/data# redis-cli -c -h redis-master-0.redis-headless -p 6379
    redis-master-0.redis-headless:6379> set 1 5
    redis-master-0.redis-headless:6379> get 1
    ```

## クラウド環境

1. API の有効化

    ```sh
    gcloud services enable compute.googleapis.com run.googleapis.com \
        cloudbuild.googleapis.com storage.googleapis.com \
        artifactregistry.googleapis.com \
        connectgateway.googleapis.com \
        anthos.googleapis.com gkeconnect.googleapis.com \
        gkehub.googleapis.com cloudresourcemanager.googleapis.com
    ```

1. 環境変数ファイルのアップロードと読み取り権限付与

    ```sh
    gcloud secrets create django_settings --data-file .env
    project_id="$( gcloud config get-value project )"
    project_num="$( gcloud projects describe "${project_id}" \
        --format='value(projectNumber)' )"
    gcloud secrets add-iam-policy-binding django_settings \
        --member "serviceAccount:${project_num}-compute@developer.gserviceaccount.com" \
        --role "roles/secretmanager.secretAccessor"
    ```

1. Django の管理者パスワードを作成

    ```sh
    echo -n "$( cat /dev/urandom | LC_ALL=C tr -dc '[:alpha:]' \
        | fold -w 30 | head -n1 )" | gcloud secrets create \
        superuser_password --data-file -
    gcloud secrets add-iam-policy-binding superuser_password \
        --member "serviceAccount:${project_num}-compute@developer.gserviceaccount.com" \
        --role "roles/secretmanager.secretAccessor"
    ```

1. GKE Autopilot クラスタの作成

    ```sh
    gcloud container clusters create-auto apps-on-googlecloud \
        --release-channel stable --region asia-northeast1
    ```

1. または [サポートされた Kubernetes クラスタ](https://cloud.google.com/anthos/docs/setup/attach-kubernetes-clusters) を Anthos に登録

    Kind の例

    ```sh
    MEMBERSHIP_NAME=kind-local
    kind create cluster --name "${MEMBERSHIP_NAME}" --image kindest/node:v1.21.10
    gcloud container hub memberships register "${MEMBERSHIP_NAME}" \
        --context $( kubectl config current-context ) \
        --kubeconfig $HOME/.kube/config \
        --enable-workload-identity \
        --has-private-issuer
    ```

    Connect Gateway への権限付与

    ```sh
    cat <<EOF | kubectl apply -f -
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      name: gateway-cluster-admin
    subjects:
    - kind: User
      name: $(gcloud config list account --format "value(core.account)")
    - kind: User
      name: ${project_num}-compute@developer.gserviceaccount.com
    roleRef:
      kind: ClusterRole
      name: cluster-admin
      apiGroup: rbac.authorization.k8s.io
    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRole
    metadata:
      name: gateway-impersonate
    rules:
    - apiGroups: [""]
      resourceNames:
      - $(gcloud config list account --format 'value(core.account)')
      - ${project_num}-compute@developer.gserviceaccount.com
      resources: ["users"]
      verbs: ["impersonate"]
    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      name: gateway-impersonate
    roleRef:
      kind: ClusterRole
      name: gateway-impersonate
      apiGroup: rbac.authorization.k8s.io
    subjects:
    - kind: ServiceAccount
      name: connect-agent-sa
      namespace: gke-connect
    EOF
    ```

    Container Registry へのアクセス限付与

    ```sh
    gcloud iam service-accounts create sa-gcr
    gcloud projects add-iam-policy-binding "${project_id}" \
        --member "serviceAccount:sa-gcr@${project_id}.iam.gserviceaccount.com" \
        --role "roles/storage.objectViewer"
    gcloud iam service-accounts keys create credential.json \
        --iam-account "sa-gcr@${project_id}.iam.gserviceaccount.com"
    kubectl create secret docker-registry container-registry \
        --docker-server asia.gcr.io \
        --docker-username _json_key \
        --docker-password "$(cat credential.json)" \
        --docker-email "sa-gcr@${project_id}.iam.gserviceaccount.com"
    kubectl -n production create secret docker-registry container-registry \
        --docker-server asia.gcr.io \
        --docker-username _json_key \
        --docker-password "$(cat credential.json)" \
        --docker-email "sa-gcr@${project_id}.iam.gserviceaccount.com"
    ```

## CI/CD

1. Cloud Deploy にパイプラインを作成

    ```sh
    vi deploy/clouddeploy.yaml
    gcloud deploy apply --file deploy/clouddeploy.yaml --region asia-northeast1
    ```

1. GitLab に渡すサービスアカウントの作成

    ```sh
    gcloud iam service-accounts create sa-gitlab
    gcloud projects add-iam-policy-binding "${project_id}" \
        --member "serviceAccount:sa-gitlab@${project_id}.iam.gserviceaccount.com" \
        --role "roles/iam.serviceAccountUser"
    gcloud projects add-iam-policy-binding "${project_id}" \
        --member "serviceAccount:sa-gitlab@${project_id}.iam.gserviceaccount.com" \
        --role "roles/storage.admin"
    gcloud projects add-iam-policy-binding "${project_id}" \
        --member "serviceAccount:sa-gitlab@${project_id}.iam.gserviceaccount.com" \
        --role "roles/run.admin"
    gcloud projects add-iam-policy-binding "${project_id}" \
        --member "serviceAccount:sa-gitlab@${project_id}.iam.gserviceaccount.com" \
        --role "roles/artifactregistry.writer"
    gcloud projects add-iam-policy-binding "${project_id}" \
        --member "serviceAccount:sa-gitlab@${project_id}.iam.gserviceaccount.com" \
        --role "roles/clouddeploy.releaser"
    gcloud secrets add-iam-policy-binding django_settings \
        --member "serviceAccount:sa-gitlab@${project_id}.iam.gserviceaccount.com" \
        --role "roles/secretmanager.secretAccessor"
    gcloud secrets add-iam-policy-binding superuser_password \
        --member "serviceAccount:sa-gitlab@${project_id}.iam.gserviceaccount.com" \
        --role "roles/secretmanager.secretAccessor"
    gcloud iam service-accounts keys create credential.json \
        --iam-account "sa-gitlab@${project_id}.iam.gserviceaccount.com"
    cat credential.json
    ```

1. GitLab CI/CD に変数を設定

    - GOOGLE_CLOUD_PROJECT_ID: プロジェクト ID
    - GOOGLE_CLOUD_REGION: リージョン asia-northeast1
    - GOOGLE_CLOUD_SA_KEY: 直前に作ったサービスアカウントのキー JSON

1. Workload Identity の設定

    ```sh
    kubectl create namespace production
    kubectl create serviceaccount django-apps --namespace default
    kubectl create serviceaccount django-apps --namespace production
    gcloud iam service-accounts create django-apps
    gcloud secrets add-iam-policy-binding django_settings \
        --member "serviceAccount:django-apps@${project_id}.iam.gserviceaccount.com" \
        --role roles/secretmanager.secretAccessor
    gcloud secrets add-iam-policy-binding superuser_password \
        --member "serviceAccount:django-apps@${project_id}.iam.gserviceaccount.com" \
        --role roles/secretmanager.secretAccessor
    gcloud iam service-accounts add-iam-policy-binding \
        "django-apps@${project_id}.iam.gserviceaccount.com" \
        --role roles/iam.workloadIdentityUser \
        --member "serviceAccount:${project_id}.svc.id.goog[default/django-apps]"
    gcloud iam service-accounts add-iam-policy-binding \
        "django-apps@${project_id}.iam.gserviceaccount.com" \
        --role roles/iam.workloadIdentityUser \
        --member "serviceAccount:${project_id}.svc.id.goog[production/django-apps]"
    kubectl annotate serviceaccount django-apps --namespace default \
        iam.gke.io/gcp-service-account=django-apps@${project_id}.iam.gserviceaccount.com
    kubectl annotate serviceaccount django-apps --namespace production \
        iam.gke.io/gcp-service-account=django-apps@${project_id}.iam.gserviceaccount.com
    kubectl patch serviceaccount django-apps -n default \
        -p "{\"imagePullSecrets\": [{\"name\": \"container-registry\"}]}"
    kubectl patch serviceaccount django-apps -n production \
        -p "{\"imagePullSecrets\": [{\"name\": \"container-registry\"}]}"
    ```

1. GitLab からの Slack 通知

    - https://api.slack.com/apps?new_app= を開きます
    - gitlab-notification と名前をつけ
    - Incoming Webhooks を有効化します
    - 新しく Webhook を追加し、連携したいチャンネルを選択
    - 生成された Webhook URL をコピーします
    - Gitlab で Setting > Integrations を選択し Slack notifications を開きます
    - 連携したい項目をチェックし、Webhook に URL をペースト、保存します

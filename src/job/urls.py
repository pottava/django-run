from django.urls import path

from . import views


urlpatterns = [
    path('', views.create, name="create"),
    path("list", views.index, name="list"),
    path('<uuid:id>', views.edit, name='edit'),
]

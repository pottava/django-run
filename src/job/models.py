import uuid
from django.db import models


class Job(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    model_type = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)

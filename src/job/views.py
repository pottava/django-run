import json
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.core import serializers
from .models import Job


def index(request):
    jobs = Job.objects.all()
    data = serializers.serialize('json', jobs)
    return HttpResponse(data, content_type="application/json")


def create(request: HttpRequest):
    record = Job(model_type=0)
    record.save()
    return redirect(to='edit', id=record.pk)


def edit(request, id):
    job = get_object_or_404(Job, id=id)
    print(json.loads(serializers.serialize('json', [job]))[0])
    return HttpResponse('Job (ID: {}, created at {})'.format(job.id, job.created))

from __future__ import absolute_import, unicode_literals
from celery import shared_task
from celery.utils.log import get_task_logger
from kubernetes import client, config
import time
import datetime


logger = get_task_logger(__name__)
t_delta = datetime.timedelta(hours=9)
JST = datetime.timezone(t_delta, 'JST')


@shared_task
def add(x, y):
    z = x + y
    time.sleep(3)

    # Kubernets Job の作成
    now = datetime.datetime.now(JST)
    job = create_job_object(
        name=f"job-{x}-{y}-{now:%Y%m%d%H%M%S}",
        image='busybox', args=['sleep', f"{y}"])

    config.load_incluster_config()
    create_job(client.BatchV1Api(), job)

    logger.info("Completed successfully")
    return z


def create_job_object(name='job', image='busybox', args=[]):
    container = client.V1Container(
        name='main',
        image=image,
        args=args,
        resources=client.V1ResourceRequirements(
            limits={
                'cpu': '500m',
                'memory': '128Mi'
            }))
    template = client.V1PodTemplateSpec(
        spec=client.V1PodSpec(
            containers=[container],
            restart_policy='Never'))
    job = client.V1Job(
        api_version='batch/v1',
        kind='Job',
        metadata=client.V1ObjectMeta(name=name),
        spec=client.V1JobSpec(
            template=template,
            parallelism=1,
            backoff_limit=3))
    return job


def create_job(api, job):
    res = api.create_namespaced_job(
        body=job,
        namespace='default')
    print("Job created. status='%s'" % str(res.status))

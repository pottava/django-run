from django.urls import path

from . import views


urlpatterns = [
    path('celery', views.celery),
    path("", views.index, name="index"),
]

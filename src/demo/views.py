from django.http import HttpRequest, HttpResponse
from django_celery_results.models import TaskResult
from django.template import loader
from .tasks import add


def index(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Hello, world!")


def celery(requests):
    template = loader.get_template('main/celery.html')
    if 'add_button' in requests.POST:
        x = int(requests.POST['input_a'])
        y = int(requests.POST["input_b"])
        task_id = add.delay(x, y)
        print(f"Task: {task_id}")

    result = list(TaskResult.objects.all().values_list("result", flat=True))
    if len(result) == 0:
        result = [0]
    context = {'result': result[0]}
    return HttpResponse(template.render(context, requests))

import os
from django.contrib import admin
from django.urls import include, path, re_path
from django.http import HttpResponse
from django.conf import settings
from django.contrib.staticfiles import views


def version(request):
    version = os.environ.get("APP_VERSION", "unknown")
    # for Cloud Run
    # revision = os.environ.get("K_REVISION", "local")
    # return HttpResponse(f"{version}-{revision}", content_type="text/plain")
    return HttpResponse(f"{version}", content_type="text/plain")


urlpatterns = [
    path("health", version),
    path("admin/", admin.site.urls),
    path("", include("demo.urls")),
    path("jobs/", include("job.urls")),
]

if settings.DEBUG:
    urlpatterns += [
        re_path(r'^static/(?P<path>.*)$', views.serve),
    ]
